import sys
import os
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QIcon
from app.main_window import VideoToGifConverter

if __name__ == "__main__":
    app = QApplication(sys.argv)

    # 设置应用程序图标
    current_dir = os.path.dirname(os.path.abspath(__file__))
    # 拼接图标文件的绝对路径
    icon_path = os.path.join(current_dir, "app", "resources", "icons", "icon.png")
    if os.path.exists(icon_path):
        app.setWindowIcon(QIcon(icon_path))
    else:
        print(f"图标文件未找到: {icon_path}")

    window = VideoToGifConverter()
    window.show()
    sys.exit(app.exec_())