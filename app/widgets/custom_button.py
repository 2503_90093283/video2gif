from PyQt5.QtWidgets import QPushButton

class CustomButton(QPushButton):
    def __init__(self, text, parent=None):
        super().__init__(text, parent)
        self.setStyleSheet("""
            QPushButton {
                background-color: #E3F2FD;
                color: #1976D2;
                border: 1px solid #90CAF9;
                padding: 5px;
                border-radius: 4px;
                font-size: 14px;
                min-width: 30px;
                min-height: 30px;
            }
            QPushButton:hover {
                background-color: #BBDEFB;
                border: 1px solid #64B5F6;
            }
            QPushButton:pressed {
                background-color: #90CAF9;
            }
        """)