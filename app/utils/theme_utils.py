LIGHT_THEME_CSS = """
/* 浅色主题样式 */
QWidget {
    background-color: #f5f5f5;
    color: #333333;
    font-family: "Segoe UI", sans-serif;
    font-size: 14px;
}

/* 输入框样式 */
QLineEdit {
    background-color: #ffffff;
    border: 1px solid #cccccc;
    border-radius: 5px;
    padding: 6px;
    color: #333333;
    font-size: 14px;
}
QLineEdit:focus {
    border: 1px solid #0078d7;
}

/* 按钮样式 */
QPushButton {
    background-color: #0078d7;
    border: 1px solid #005bb5;
    border-radius: 5px;
    padding: 8px 16px;
    color: #ffffff;
    font-size: 14px;
    font-weight: bold;
}
QPushButton:hover {
    background-color: #005bb5;
    border-color: #004080;
}
QPushButton:pressed {
    background-color: #004080;
}
QPushButton:disabled {
    background-color: #cccccc;
    color: #666666;
}

/* 标签样式 */
QLabel {
    color: #333333;
    font-size: 14px;
    font-weight: bold;
}

/* 复选框样式 */
QCheckBox {
    color: #333333;
    font-size: 14px;
}
QCheckBox::indicator {
    width: 16px;
    height: 16px;
}
QCheckBox::indicator:checked {
    background-color: #0078d7;
    border: 1px solid #005bb5;
}
QCheckBox::indicator:unchecked {
    background-color: #ffffff;
    border: 1px solid #cccccc;
}
"""

DARK_THEME_CSS = """
/* 深色主题样式 */
QWidget {
    background-color: #2d2d2d;
    color: #ffffff;
    font-family: "Segoe UI", sans-serif;
    font-size: 14px;
}

/* 输入框样式 */
QLineEdit {
    background-color: #3a3a3a;
    border: 1px solid #555555;
    border-radius: 5px;
    padding: 6px;
    color: #ffffff;
    font-size: 14px;
}
QLineEdit:focus {
    border: 1px solid #0078d7;
}

/* 按钮样式 */
QPushButton {
    background-color: #0078d7;
    border: 1px solid #005bb5;
    border-radius: 5px;
    padding: 8px 16px;
    color: #ffffff;
    font-size: 14px;
    font-weight: bold;
}
QPushButton:hover {
    background-color: #005bb5;
    border-color: #004080;
}
QPushButton:pressed {
    background-color: #004080;
}
QPushButton:disabled {
    background-color: #555555;
    color: #999999;
}

/* 标签样式 */
QLabel {
    color: #ffffff;
    font-size: 14px;
    font-weight: bold;
}

/* 复选框样式 */
QCheckBox {
    color: #ffffff;
    font-size: 14px;
}
QCheckBox::indicator {
    width: 16px;
    height: 16px;
}
QCheckBox::indicator:checked {
    background-color: #0078d7;
    border: 1px solid #005bb5;
}
QCheckBox::indicator:unchecked {
    background-color: #3a3a3a;
    border: 1px solid #555555;
}
"""