import unittest
from app.main_window import VideoToGifConverter

class TestMainWindow(unittest.TestCase):
    def test_window_initialization(self):
        window = VideoToGifConverter()
        self.assertIsNotNone(window)

if __name__ == "__main__":
    unittest.main()