import os
from pathlib import Path

PROJECT_STRUCTURE = {
    "video2gif": {
        "main.py": "",
        "requirements.txt": "",
        "README.md": "",
        ".gitignore": "",
        "app": {
            "__init__.py": "",
            "main_window.py": "",
            "utils": {
                "__init__.py": "",
                "file_utils.py": "",
                "theme_utils.py": "",
                "logging_utils.py": "",
            },
            "widgets": {
                "__init__.py": "",
                "custom_button.py": "",
                "custom_slider.py": "",
            },
            "dialogs": {
                "__init__.py": "",
                "settings_dialog.py": "",
                "about_dialog.py": "",
            },
            "models": {
                "__init__.py": "",
                "data_model.py": "",
            },
            "controllers": {
                "__init__.py": "",
                "main_controller.py": "",
            },
            "resources": {
                "icons": {},
                "styles": {},
                "translations": {},
            },
        },
        "tests": {
            "__init__.py": "",
            "test_main_window.py": "",
            "test_utils.py": "",
        },
        "scripts": {
            "generate_project.py": "",
            "setup.py": "",
        },
    }
}

def create_structure(base_path, structure):
    for name, content in structure.items():
        path = base_path / name
        if isinstance(content, dict):
            path.mkdir(parents=True, exist_ok=True)
            create_structure(path, content)
        else:
            path.touch()

def main():
    project_name = "video2gif"
    base_path = Path(project_name)
    if base_path.exists():
        print(f"项目 '{project_name}' 已存在！")
        return
    create_structure(base_path, PROJECT_STRUCTURE)
    print(f"项目 '{project_name}' 已成功生成！")

if __name__ == "__main__":
    main()