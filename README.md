# Video to GIF Converter

## 简介
**Video to GIF Converter** 是一款简单易用的工具，支持将视频文件（如 MP4、AVI、MOV 等）转换为高质量的 GIF 动画。适用于设计师、开发者及普通用户，轻松制作 GIF 并应用于社交媒体、演示文稿或网页。
![gif.gif](https://raw.gitcode.com/2503_90093283/video2gif/attachment/uploads/a23ffc22-981c-4a26-b347-9a45476b4461/gif.gif 'gif.gif')


---

## 功能概述
1. **视频转 GIF**：
   - 支持 MP4、AVI、MOV、MKV、FLV、WEBM、WMV 和 GIF 格式。
   - 优化算法，生成体积小、画质清晰的 GIF。
2. **视频剪辑**：
   - 自定义起始和结束时间，精确截取片段。
   - 调整帧率（FPS），控制动画流畅度。
3. **分辨率调整**：
   - 自定义 GIF 尺寸，支持保持宽高比。
4. **主题切换**：
   - 支持浅色和深色主题，自动适配系统设置。
5. **多语言支持**：
   - 中英文切换，自动检测系统语言。
6. **快捷键操作**：
   - 空格键播放/暂停，左右键快进/快退，`Ctrl+S` 快速导出。

---

## 快速开始

### 1. 安装
1. **安装 Python**：
   - 下载 [Python 3.7+](https://www.python.org/downloads/)。
   - 确保勾选“Add Python to PATH”。
2. **安装依赖**：
   - 克隆项目：
     ```bash
     git clone https://gitee.com/linyuncode/video2gif
     cd video-to-gif-converter
     ```
   - 安装依赖包：
     ```bash
     pip install -r requirements.txt
     ```

### 2. 使用步骤
1. **启动程序**：
   ```bash
   python main.py
   ```
2. **打开视频**：
   - 点击“文件” > “打开视频”，选择视频文件。
3. **设置参数**：
   - 调整起始时间、结束时间、分辨率、帧率等。
   - 勾选“优化 GIF”以减小文件体积。
4. **导出 GIF**：
   - 点击“导出 GIF”，选择保存路径并开始转换。

### 3. 打包为可执行文件（可选）
1. 安装 PyInstaller：
   ```bash
   pip install pyinstaller
   ```
2. 打包程序：
   ```bash
   pyinstaller --windowed --icon=app/resources/icons/icon.png --add-data "app/resources/icons/icon.png;app/resources/icons" main.py
   ```
3. 运行可执行文件：
   - 进入 `dist/main/` 目录，双击 `main.exe`（Windows）或 `main`（macOS/Linux）。

---

## 常见问题
1. **GIF 文件体积过大**：
   - 勾选“优化 GIF”选项，或降低分辨率、帧率。
2. **如何调整分辨率**：
   - 在“宽度”和“高度”输入框中设置。
3. **视频播放卡顿**：
   - 降低预览分辨率或帧率。

---

**Happy Converting!** 🎉